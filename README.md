# Project Peer to Peer JAVA group C

### UPSSITECH - STRI 2A
- Nicolas SARLAT
- Dorian GARDES

---

Les différentes étapes du projet sont disponibles dans la section [Releases](https://gitlab.com/up6tech/s7_java_parallel/project-group-c/-/releases).

#### Outils utilisés:
- Java 17 (non testé avec des versions précédentes)
- IntelliJ IDEA
- PlantUML