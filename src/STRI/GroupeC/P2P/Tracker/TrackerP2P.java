package STRI.GroupeC.P2P.Tracker;

import STRI.GroupeC.FTP.Models.Networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TrackerP2P {

    static AnnuaireP2P annuaire; // Liste des nodes connus
    static boolean isCountEnabled;

    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("Usage: TrackerP2P [Port d'écoute] <Activer équitée (activé par défaut)>");
            return;
        }
        ServerSocket socketEcoute;
        TrackerP2P.annuaire = new AnnuaireP2P();

        if (args.length > 2) {
            TrackerP2P.isCountEnabled = Boolean.parseBoolean(args[1]);
        } else TrackerP2P.isCountEnabled = true;

        try {
            socketEcoute = new ServerSocket(Integer.parseInt(args[0]));
        } catch (NumberFormatException ignored) {
            System.out.println("Merci d'entrer un nombre comme port d'écoute");
            return;
        } catch (IOException e) {
            try { socketEcoute = new ServerSocket(0);} // Si ça échoue on laisse l'OS choisir
            catch (IOException ex) {
                System.out.println("Impossible d'ouvrir un socket d'écoute: " + ex.getMessage());
                return;
            }
        }
        System.out.println("Serveur en écoute sur le port: " + socketEcoute.getLocalPort());

        ServerSocket finalSocketEcoute = socketEcoute;
        new Thread(() -> {
            try {
                while (true) {
                    Socket socketClient = finalSocketEcoute.accept();
                    new Thread(() -> {
                        try { gestionNodes(socketClient); }
                        catch (IOException e) { e.printStackTrace(); }
                    }).start();
                }
            } catch (IOException ignored) {}
        }).start();
    }

    /**
     * Méthode appellée par le Thread de gestion des nodes.
     * @param socket socket de connexion avec le client
     * @throws IOException en cas de problème de lecture du socket
     */
    public static void gestionNodes(Socket socket) throws IOException {

        // On instancie les objets pour manipuler le socket
        BufferedReader entreeClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintStream sortieClient = new PrintStream(socket.getOutputStream());

        String commandeClient; // Première lecture de l'input

        // On boucle tant que le client n'as pas fermé son côté du socket
        do {
            commandeClient = Networking.lireSocket(entreeClient); // Lecture de l'input
            //System.out.println("avant " + commandeClient + " " + Arrays.toString(TrackerP2P.annuaire.get().toArray()));
            Networking.ecrireSocket(sortieClient, TrackerProtocol.commandParser(commandeClient, new InetSocketAddress(socket.getInetAddress(), socket.getPort()))); // Traitement de la requête
            //System.out.println("après " + commandeClient + " " + Arrays.toString(TrackerP2P.annuaire.get().toArray()));
        } while(!commandeClient.equals("")); // Socket fermé côté Node
        // On a fini on ferme proprement
        socket.close();
    }
}
