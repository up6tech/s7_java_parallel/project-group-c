package STRI.GroupeC.P2P.Tracker;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;

public class AnnuaireP2P {

    /**
     * Liste des nodes connus
     */
    private final HashMap<InetSocketAddress, NodeInformation> nodesP2P;

    /**
     * Création de l'annuaire
     */
    public AnnuaireP2P() { this.nodesP2P = new HashMap<>(); }

    /**
     * Ajout d'un nodeP2P à l'annuaire
     * @param addr Adresse du node
     * @param port Port du node
     */
    public synchronized void addNode(InetAddress addr, int port) {
        this.nodesP2P.put(new InetSocketAddress(addr, port), new NodeInformation(addr, port));
    }

    /**
     * Enlèvement d'un nodeP2P à l'annuaire
     * @param addr Adresse du node
     * @param port Port du node
     */
    public synchronized void removeNode(InetAddress addr, int port) {
        this.nodesP2P.remove(new InetSocketAddress(addr, port));
    }

    public synchronized NodeInformation getNodeInformation(InetAddress addr, int port) {
        return this.getNodeInformation(new InetSocketAddress(addr, port));
    }

    public synchronized NodeInformation getNodeInformation(InetSocketAddress addr) {
        return this.nodesP2P.get(addr);
    }

    /**
     * Récupération de la liste des nodes connus
     * @return liste des nodes
     */
    public ArrayList<InetSocketAddress> get() {

        ArrayList<InetSocketAddress> nodesAddr = new ArrayList<>();
        this.nodesP2P.forEach((nodeId, node) -> nodesAddr.add(node.getAddr()));
        return nodesAddr;
    }


}
