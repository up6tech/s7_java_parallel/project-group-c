package STRI.GroupeC.P2P.Tracker;

import STRI.GroupeC.FTP.Models.Networking;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

public class TrackerProtocol {

    private static final Random randomGenerator = new SecureRandom();

    /**
     * Message affiché en cas de demande d'aide
     */
    private static final String HELP_MSG =
"""
Available commands:
JOIN    Join the P2P network
LEAVE   Leave the P2P network
RETR    Ask where to download
WHERE   Ask wich node to request
""";

    /**
     * Point d'entrée du traitement du protocole tracker
     * @param command Chaine de charactère reçue du client
     * @param sender Adresse du node originaire de la requête
     * @return Chaine à renvoyer au client
     * @throws RuntimeException En cas de demmande de fin de connexion l'exception comporte le message "QUIT"
     */
    public static String commandParser(String command, InetSocketAddress sender) throws RuntimeException {
        String[] request = command.split(" ", 4);
        try {
            return switch (request[0].toLowerCase()) {
                case "join" -> TrackerProtocol.editAnnuaire(sender, request[1], true);
                case "leave" -> TrackerProtocol.editAnnuaire(sender, request[1], false);
                case "retr" -> commandRETR(request, sender);
                case "where" -> commandWHERE(request, sender);
                default -> "ERROR - UnknownCommand\n" + HELP_MSG;
            };
        } catch (ArrayIndexOutOfBoundsException e) {
            return "ERROR - Missing argument";
        }
    }

    /**
     * Ajoute ou retire un node à l'annuaire
     * @param sender Addresse du node à ajouter
     * @return 'OK' ou le message d'erreur correspondant
     */
    private static String editAnnuaire(InetSocketAddress sender, String port, boolean isAjout) {
        try {
            if (isAjout) {
                Socket socket = new Socket(sender.getAddress(), Integer.parseInt(port)); // On essaie de se connecter pour verifier qu'on ne nous ment pas.
                if (socket.isConnected()) {
                    socket.close();
                    TrackerP2P.annuaire.addNode(sender.getAddress(), Integer.parseInt(port));
                    return "OK";
                } else return "ERROR - Tracker unable to connect to node";
            } else { // On retire bêtement
                TrackerP2P.annuaire.removeNode(sender.getAddress(), Integer.parseInt(port));
                return "OK";
            }
        } catch (NumberFormatException ignored) {
            return "ERROR - Port not a number";
        } catch (IOException e) {
            return "ERROR - Socket error " + e.getMessage();
        }
    }

    /**
     * Permet aux nodes de demander à quel node envoyer une requête
     * @param request Détails donnés par le node
     * @param sender Adresse du node originaire de la requête
     * @return Adresse ou liste d'adresses où demander
     */
    private static String commandWHERE(String[] request, InetSocketAddress sender) {

        final StringBuilder reponse = new StringBuilder();
        if (request[1].equalsIgnoreCase("list")) {
            ArrayList<InetSocketAddress> nodes = (ArrayList<InetSocketAddress>) TrackerP2P.annuaire.get().clone();
            nodes.remove(new InetSocketAddress(sender.getAddress(), Integer.parseInt(request[2])));
            nodes.forEach(node -> reponse.append(node.getHostName()).append(" ").append(node.getPort()).append('\n'));
            return reponse.toString();
        } else if (request[1].equalsIgnoreCase("cwd")) return TrackerProtocol.checkFileOnNodes(request[3], new InetSocketAddress(sender.getAddress(), Integer.parseInt(request[2])));
        else return "ERROR - Unknow command";
    }

    /**
     * Permet aux nodes de demander à quel node envoyer un téléchargement de fichier
     * @param request Détails donnés par le node
     * @param sender Adresse du node originaire de la requête
     * @return Adresse ou liste d'adresses où demander
     */
    private static String commandRETR(String[] request, InetSocketAddress sender) {

        return TrackerProtocol.checkFileOnNodes(request[2], new InetSocketAddress(sender.getAddress(), Integer.parseInt(request[1])), TrackerP2P.isCountEnabled);
    }

    /**
     * Permet de trouver un node disposant d'un fichier demandé par un autre node
     * @param path Chemin du fichier demandé
     * @param sender Adresse du node originaire de la requête
     * @return Adresse du node à questionner
     */
    private static String checkFileOnNodes(String path, InetSocketAddress sender) {
        return TrackerProtocol.checkFileOnNodes(path, sender, false);
    }

    /**
     * Permet de trouver un node disposant d'un fichier demandé par un autre node
     * @param path Chemin du fichier demandé
     * @param sender Adresse du node originaire de la requête
     * @param countForStats Active le comptage des paquets émis/reçus par chaque node.
     * @return Adresse du node à questionner
     */
    private static String checkFileOnNodes(String path, InetSocketAddress sender, boolean countForStats) {

        if (countForStats && TrackerP2P.annuaire.getNodeInformation(sender).getRatio() < 0.5)
            return "ERROR - Ratio too low. Send more files to have access to download";

        StringBuilder reponse = new StringBuilder();
        ArrayList<InetSocketAddress> nodes = (ArrayList<InetSocketAddress>) TrackerP2P.annuaire.get().clone();
        nodes.remove(sender); // On évite de se demander à sois même ;)

        while (nodes.size() > 0) {
            try {
                Socket socket = Networking.selectionServeur(nodes, randomGenerator); // Sélection aléatoire d'un serveur
                String nodeResponse = Networking.askToPair(socket, "HAS " + path);
                if (nodeResponse.equalsIgnoreCase("ok")) {
                    reponse.append(socket.getInetAddress().getHostName()).append(" ").append(socket.getPort()).append('\n');
                    if (countForStats) {
                        TrackerP2P.annuaire.getNodeInformation(sender).countReceived();
                        TrackerP2P.annuaire.getNodeInformation(socket.getInetAddress(), socket.getPort()).countSent();
                    }
                    return reponse.toString();
                } else {
                    nodes.remove(new InetSocketAddress(socket.getInetAddress(), socket.getPort()));
                }
            } catch (IOException ignored) {}
        }
        return "ERROR - File not found";
    }
}
