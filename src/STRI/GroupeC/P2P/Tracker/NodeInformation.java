package STRI.GroupeC.P2P.Tracker;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class NodeInformation {

    private final InetSocketAddress addr; // Address of the node
    private long sent;              // Number of packets send
    private long received;          // Number of packets received

    /**
     * Création des métadonnées d'un node
     * @param addr Adresse d'un node
     * @param port Port du node
     */
    public NodeInformation(InetAddress addr, int port) {
        this.addr = new InetSocketAddress(addr, port);
        this.sent = 0;
        this.received = 0;
    }

    /**
     * Permet de comptabiliser l'envoi de paquets par ce node
     * @param count Nombre de packets envoyés
     */
    public synchronized void countSent(long count) {
        this.sent += count;
    }

    public synchronized void countSent() {
        this.countSent(1);
    }

    /**
     * Permet de comptabiliser la réception de paquets par ce node
     * @param count Nombre de packets reçus
     */
    public synchronized void countReceived(long count) {
        this.received += count;
    }

    public synchronized void countReceived() {
        this.countReceived(1);
    }

    public InetSocketAddress getAddr() {
        return this.addr;
    }

    /**
     * Calcule le ratio entre le nombre de paquets émis et reçus
     * @return 1.0 indique que le node envoie autant qu'il ne reçois. Un nombre inférieur qu'il envoie plus qu'il ne reçois et inversement
     */
    public long getRatio() {

        if (this.sent + this.received > 50) { // On offre les 50 premiers packets puisque l'infra est toute petite pour éviter de bloquer dès les premiers paquets.
            return this.received / this.sent;
        } else return 1;
    }

    /**
     * Permet de déterminer si deux objets sont égaux.
     * (Plus utilisé depuis le passage à une HashMap dans AnnuaireP2P)
     * @param obj objet à comparer avec this
     * @return True si les objets sont identiques
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NodeInformation object) {
            return this.addr.equals(object.getAddr());
        } else return false;
    }
}

