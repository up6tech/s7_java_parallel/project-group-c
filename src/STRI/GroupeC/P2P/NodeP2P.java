package STRI.GroupeC.P2P;

import STRI.GroupeC.FTP.ClientFTP;
import STRI.GroupeC.FTP.Models.LocalFileSystem;
import STRI.GroupeC.FTP.Models.Networking;
import STRI.GroupeC.FTP.ServeurFTP;

import java.io.*;
import java.net.InetSocketAddress;

public class NodeP2P {

    public static InetSocketAddress trackerAddr;

    public static int serverListenningPort;

    public static void main(String[] args) {

        if (args.length < 2) {
            System.out.println("Usage: NodeP2P [Chemin du fichier de config] [Port d'écoute] <Chemin du dossier à partager>");
            return;
        }

        ServeurFTP serv = new ServeurFTP(Integer.parseInt(args[1]), args[2]); // On démarre la partie serveur
        NodeP2P.serverListenningPort = Integer.parseInt(args[1]);

        if (new File(args[0]).exists()) { // On lis le fichier de config
            try {
                NodeP2P.trackerAddr = LocalFileSystem.readConfig(args[0], Integer.parseInt(args[1]));
                if (NodeP2P.trackerAddr == null) {
                    NodeP2P.stopServer(serv);
                    return;
                }
            } catch (IOException e) {
                System.err.println("\nERREUR à la lecture du fichier de configuration: " + e.getMessage() + "\nRéinitialisation de la configuration.");
                try {
                    LocalFileSystem.writeConfig(args[0], null);
                } catch (IOException ignored) {}
                NodeP2P.stopServer(serv);
                return;
            }
        }
        else {
            System.err.println("\nERREUR Fichier de configuration introuvable\nCréation de la configuration.");
            try {
                LocalFileSystem.writeConfig(args[0], null);
            } catch (IOException ignored) {}
            NodeP2P.stopServer(serv);
            return;
        }


        ClientFTP.run(NodeP2P.trackerAddr);

        try { // On écrit le fichier de config
            LocalFileSystem.writeConfig(args[0], NodeP2P.trackerAddr);
        } catch (IOException e) {
            System.err.println("\nERREUR à l'écriture du fichier de configuration: " + e.getMessage());
        }

        NodeP2P.leaveTracker(Integer.parseInt(args[1]));
        NodeP2P.stopServer(serv);
    }

    private static void stopServer(ServeurFTP serv) {
        try { // On stoppe la partie serveur
            serv.stopListening();
        } catch (IOException e) {
            System.err.println("\nERREUR à la fermeture du socket serveur: " + e.getMessage());
        }
    }

    private static void leaveTracker(int port) {
        try {
            String reponse = Networking.askToPair(NodeP2P.trackerAddr, "LEAVE " + port);
            if (!reponse.equalsIgnoreCase("ok")) {
                System.err.println("Erreur lors du contact avec le tracker: " + reponse);
            }
        } catch (IOException e) {
            System.err.println("Impossible de contacter le Tracker: " + e.getMessage());
        }
    }
}
