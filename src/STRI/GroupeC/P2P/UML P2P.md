# UML Partie FTP

### Diagramme de cas d'utilisation
```plantuml
actor Utilisateur
package "Service P2P" {
  usecase "Télécharger des fichier" as UC1
  usecase "Se déplacer dans une arborescence" as UC2
  usecase "Lister les fichiers" as UC3
}
Utilisateur --> UC1
Utilisateur --> UC2
Utilisateur --> UC3
```

### Diagramme de classe
```plantuml
left to right direction
together {
    class Exception
    package "FTP" {
        package Exceptions {
            class NotADirectoryException
            class FileDoesntExistException
            class CantReadException
        }
    }
}
package "P2P" {
    class NodeP2P {
        {static} + trackerAddr : InetSocketAddress
        {static} + serverListenningPort : int
    
        + main(String[] args)
        {static} - stopServer(ServeurFTP serv)
        {static} - leaveTracker(int port)
    }
    package "Tracker" {
        class TrackerP2P {
            {static} ~ annuaire : AnnuaireP2P
            {static} ~ isCountEnabled : boolean
            
            + main(String[] args)
            {static} + gestionNodes(Socket socket)
        }
        class AnnuaireP2P {
            - nodesP2P HashMap<InetSocketAddress, NodeInformation>
            
            + addNode(InetAddress addr, int port)
            + removeNode(InetAddress addr, int port)
            + getNodeInformation(InetAddress addr, int port) : NodeInformation
            + getNodeInformation(InetSocketAddress addr) : NodeInformation
            + get() : ArrayList<InetSocketAddress>
        }
        class NodeInformation {
            - addr : InetSocketAddress
            - sent : long
            - received : long
            
            + countSent(long count)
            + countSent()
            + countReceived(long count)
            + countReceived()
            + getAddr() : InetSocketAddress
            + getRatio() : long
        }
        class TrackerProtocol {
            {static} - randomGenerator : SecureRandom
            {static} - HELP_MSG : String
                        
            {static} + commandParser(String command, InetSocketAddress sender) : String
            {static} - editAnnuaire(InetSocketAddress sender, String port, boolean isAjout) : String
            {static} - commandWHERE(String[] request, InetSocketAddress sender) : String 
            {static} - commandRETR(String[] request, InetSocketAddress sender) : String
            {static} - checkFileOnNodes(String path, InetSocketAddress sender) : String 
            {static} - checkFileOnNodes(String path, InetSocketAddress sender,  boolean countForStats) : String  
        }
    }
}
package "FTP" {

    class ServeurFTP {
        - socketEcoute : ServerSocket
    
        + ServeurFTP(int port, String sharePath)
        + stopListening()
        {static} + gestionClient(Socket socket)
    }
    
    class ClientFTP {
        {static} + run(InetSocketAddress tracker)
        {static} - listAllRemoteFolders(InetSocketAddress tracker, String commandeType, String remotePath, int port)
        {static} - gestionDownloads(InetSocketAddress tracker, String path, String fileName)
    }
    
    package "Models" {
    
        class LocalFileSystem {
            {static} + SHARE_FOLDER_PATH : String
        
            {static} + listFiles(String workingDir) : File[]
            {static} + getFile(String filePath) : File
            {static} + checkDirectoryReadable(String path) : boolean
            {static} + checkChangeDirectory(String path)
            {static} + readBlockFromFile(long blockNbr, File fichier) : byte[]
            {static} + writeConfig(String path, InetSocketAddress addr)
            {static} + readConfig(String path, int port) : InetSocketAddress
        }
        
        class FTProtocol {
            {static} - HELP_MSG : String
            
            {static} + commandParser(String command) : String
            {static} - commandLIST(String workingDir) : String
            {static} - commandCWD(String newFolder) : String
            {static} - commandRETR(String request) : String
            {static} - commandHAS(String path) : String
        }
        
        class Networking {
            {static} + PACKET_SIZE : int
        
            {static} + lireSocket(BufferedReader streamToRead) : String
            {static} + ecrireSocket(PrintStream streamToWrite, String message)
            {static} + selectionServeur(List<InetSocketAddress> serveurs, Random randomGenerator) : Socket
            {static} + askToPair(InetSocketAddress pairAddr, String message) : String
            {static} + askToPair(Socket socket, String message) : String
            {static} + parseSocketAddr(String line) : InetSocketAddress
            {static} + parseSocketAddr(String[] lines) : ArrayList<InetSocketAddress>
            {static} + downloadDataBlock(InetAddress addr, int port) : byte[]
        }
    }
}
NotADirectoryException <|-- Exception
FileDoesntExistException <|-- Exception
CantReadException <|-- Exception
```

### Diagramme de séquence : Demande de pair à un tracker P2P
```plantuml
@startuml
autonumber
actor NodeP2P
NodeP2P -> ClientFTP
activate ClientFTP
ClientFTP -> TrackerP2P : "Connexion au socket d'écoute"
activate TrackerP2P
TrackerP2P -> ClientFTP: socket.accept()
TrackerP2P -> Thread: gestionNodes(socket)
deactivate TrackerP2P
activate Thread
loop while !socket.isInputShutdown()
    ClientFTP -> Thread: "WHERE CWD 21 /Bureau"
    Thread -> Networking: lireSocket(entreeSocketClient)
    activate Networking
    Networking -> Thread: "WHERE CWD 21 /Bureau" 
    deactivate Networking
    Thread -> TrackerProtocol: commandParser(command)
    activate TrackerProtocol
    TrackerProtocol -> TrackerProtocol: commandWHERE(request, sender)
    activate TrackerProtocol
    TrackerProtocol -> TrackerProtocol: TrackerProtocol.checkFileOnNodes(request, sender)
    activate TrackerProtocol
    TrackerProtocol -> Networking: Networking.selectionServeur(nodes, randomGenerator)
    activate Networking
    Networking -> TrackerProtocol
    deactivate Networking
    TrackerProtocol -> Networking: Networking.askToPair(socket, "HAS " + path);
    activate Networking
    Networking -> Networking: Networking.ecrireSocket(ecritureSocket, message);
    activate Networking
    Networking -> ServeurFTP: "HAS /Bureau"
    alt "/Bureau".exists()
        activate ServeurFTP
        ServeurFTP -> Networking: "OK"
        deactivate ServeurFTP
        Networking -> TrackerProtocol: "OK"
        deactivate Networking
        deactivate TrackerProtocol
        deactivate TrackerProtocol
        TrackerProtocol -> Thread: "example.com 21"
        deactivate TrackerProtocol
        Thread -> Networking: Networking.ecrireSocket(sortieClient, "example.com 21")
        activate Networking
        Networking -> ClientFTP: "example.com 21"
        deactivate Networking
    else
        activate ServeurFTP
        ServeurFTP -> Networking: "NO"
        deactivate ServeurFTP
        Networking -> TrackerProtocol: "NO"
        deactivate Networking
        deactivate TrackerProtocol
        deactivate TrackerProtocol
        TrackerProtocol -> Thread: "Error - File Not Found"
        deactivate TrackerProtocol
        Thread -> Networking: Networking.ecrireSocket(sortieClient, "Error - File Not Found")
        activate Networking
        Networking -> ClientFTP: "Error - File Not Found"
        deactivate Networking
    end
end
Thread --> TrackerP2P
deactivate Thread
ClientFTP -> NodeP2P
deactivate ClientFTP
@enduml
```

Une fois les informations du pair envoyées au NodeP2P la requête est exécutée comme [indiqué précédement](https://gitlab.com/up6tech/s7_java_parallel/project-group-c/-/blob/main/src/STRI/GroupeC/FTP/UML%20FTP.md).