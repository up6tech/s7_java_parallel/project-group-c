# UML Partie FTP

### Diagramme de cas d'utilisation
```plantuml
actor Utilisateur
package "Service FTP" {
  usecase "Télécharger des fichier" as UC1
  usecase "Se déplacer dans une arborescence" as UC2
  usecase "Lister les fichiers" as UC3
}
Utilisateur --> UC1
Utilisateur --> UC2
Utilisateur --> UC3
```

### Diagramme de classe
```plantuml
left to right direction
together {
    class Exception
    package "FTP" {
        package Exceptions {
            class NotADirectoryException
            class FileDoesntExistException
            class CantReadException
        }
    }
}
package "FTP" {

    class ServeurFTP {
        + main(String[] args)
        {static} + gestionClient(Socket socket)
    }
    
    class ClientFTP {
        {static} - randomGenerator : Random
    
        + main(String[] args)
        {static} - gestionDownloads(ArrayList<InetSocketAddress> serveurs, String path, String fileName)
    }
    
    package "Models" {
    
        class LocalFileSystem {
            {static} + BASE_PATH : String
        
            {static} + listFiles(String workingDir) : File[]
            {static} + getFile(String filePath) : File
            {static} + checkDirectoryReadable(String path) : boolean
            {static} + readBlockFromFile(long blockNbr, File fichier) : byte[]
        }
        
        class FTProtocol {
            {static} - HELP_MSG : String
            {static} + FTP_DEFAULT_PORT : int
            
            {static} + commandParser(String command) : String
            {static} - commandLIST(String workingDir) : String
            {static} - commandCWD(String newFolder) : String
            {static} - commandRETR(String request) : String  
        }
        
        class Networking {
            {static} + PACKET_SIZE : int
        
            {static} + getAddress(Scanner clavier) : InetAddress
            {static} + lireSocket(BufferedReader streamToRead) : String
            {static} + ecrireSocket(PrintStream streamToWrite, String message)
            {static} + selectionServeur(List<InetSocketAddress> serveurs, Random randomGenerator) : Socket
            {static} + downloadDataBlock(InetAddress addr, int port) : byte[]
        }
    }
}
NotADirectoryException <|-- Exception
FileDoesntExistException <|-- Exception
CantReadException <|-- Exception
```

### Diagramme de séquence : Se déplacer dans l'arborescence
```plantuml
@startuml
autonumber
actor ClientFTP
ClientFTP -> ServeurFTP: "Connexion au socket d'écoute"
activate ServeurFTP
ServeurFTP --> ClientFTP: socket.accept()
ServeurFTP -> Thread: gestionClient(socket)
deactivate ServeurFTP
activate Thread
loop while !socket.isInputShutdown()
    ClientFTP -> Thread: "CWD /Bureau"
    Thread -> FTProtocol: lireSocket(entreeSocketClient)
    activate FTProtocol
    FTProtocol -> Thread: "CWD /Bureau" 
    deactivate FTProtocol
    Thread -> FTProtocol: commandParser(command)
    activate FTProtocol
    FTProtocol -> FTProtocol: commandCWD(newFolder)
    FTProtocol -> LocalFileSystem: checkChangeDirectory(newFolder);
    activate LocalFileSystem
    alt newFolder.exists()
        alt newFolder.isDirectory()
            alt newFolder.canRead()
                LocalFileSystem -> FTProtocol
                deactivate LocalFileSystem
                FTProtocol --> Thread: "OK"
                deactivate FTProtocol
                Thread --> ClientFTP: "OK"
            else
                activate LocalFileSystem
                activate FTProtocol
                LocalFileSystem --> FTProtocol: CantReadException
                destroy LocalFileSystem
                FTProtocol --> Thread: "ERROR - Can't read folder content"
                deactivate FTProtocol
                Thread --> ClientFTP: "ERROR - Can't read folder content"
            end
        else
            activate LocalFileSystem
            activate FTProtocol
            LocalFileSystem --> FTProtocol: NotADirectoryException
            destroy LocalFileSystem
            FTProtocol --> Thread: "ERROR - This is not a folder"
            deactivate FTProtocol
            Thread --> ClientFTP: "ERROR - This is not a folder"
        end
    else
        activate LocalFileSystem 
        activate FTProtocol
        LocalFileSystem --> FTProtocol: FileDoesntExistException
        destroy LocalFileSystem
        FTProtocol --> Thread: "ERROR - Folder not found"
        deactivate FTProtocol
        Thread --> ClientFTP: "ERROR - Folder not found"
    end
end
'deactivate ClientFTP
Thread --> ServeurFTP
deactivate Thread
@enduml
```

### Diagramme de séquence : Téléchargement d'un block de fichier
```plantuml
@startuml
autonumber
actor ClientFTP
ClientFTP -> ServeurFTP: "Connexion au socket d'écoute"
activate ServeurFTP
ServeurFTP --> ClientFTP: socket.accept()
ServeurFTP -> Thread: gestionClient(socket)
deactivate ServeurFTP
activate Thread
loop while !socket.isInputShutdown()
    ClientFTP -> Thread: "RETR 1 /Fichier.txt"
    Thread -> FTProtocol: lireSocket(entreeSocketClient)
    activate FTProtocol
    FTProtocol -> Thread: "RETR 1 /Fichier.txt" 
    deactivate FTProtocol
    Thread -> FTProtocol: commandParser(command)
    activate FTProtocol
    FTProtocol -> FTProtocol: commandRETR(request)
    FTProtocol -> LocalFileSystem: getFile(filePath)
    activate LocalFileSystem
    
    'if (!fichier.exists()) return "ERROR - File not found";
    'else if (fichier.isDirectory()) return "ERROR - This is a folder";
    'else if (!fichier.canRead()) return "ERROR - Can't read file content";
    
    alt newFolder.exists()
        alt newFolder.isDirectory()
            alt newFolder.canRead()
                LocalFileSystem --> FTProtocol
                deactivate LocalFileSystem
                FTProtocol -> ThreadTransfert: new Thread()
                activate ThreadTransfert
                FTProtocol --> Thread: "OK_DATA_PORT 12345"
                deactivate FTProtocol
                Thread --> ClientFTP: "OK_DATA_PORT 12345"
                note right
                    Indique le numéro de port du socket de transfert
                end note
                ClientFTP -> ThreadTransfert: "Connexion au socket de transfert"
                ThreadTransfert --> ClientFTP: socket.accept()
                ThreadTransfert --> ClientFTP: binary block "01000111010101001010101010"
                ThreadTransfert --> ClientFTP: socket.close()
                ClientFTP --> ThreadTransfert: socket.close()
                deactivate ThreadTransfert
            else
                activate LocalFileSystem
                activate FTProtocol
                LocalFileSystem --> FTProtocol: CantReadException
                destroy LocalFileSystem
                FTProtocol --> Thread: "ERROR - Can't read folder content"
                deactivate FTProtocol
                Thread --> ClientFTP: "ERROR - Can't read folder content"
            end
        else
            activate LocalFileSystem
            activate FTProtocol
            LocalFileSystem --> FTProtocol: NotADirectoryException
            destroy LocalFileSystem
            FTProtocol --> Thread: "ERROR - This is not a folder"
            deactivate FTProtocol
            Thread --> ClientFTP: "ERROR - This is not a folder"
        end
    else
        activate LocalFileSystem 
        activate FTProtocol
        LocalFileSystem --> FTProtocol: FileDoesntExistException
        destroy LocalFileSystem
        FTProtocol --> Thread: "ERROR - Folder not found"
        deactivate FTProtocol
        Thread --> ClientFTP: "ERROR - Folder not found"
    end
end
'deactivate ClientFTP
Thread --> ServeurFTP
deactivate Thread
@enduml
```