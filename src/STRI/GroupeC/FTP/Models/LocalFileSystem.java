package STRI.GroupeC.FTP.Models;

import STRI.GroupeC.FTP.Exceptions.CantReadException;
import STRI.GroupeC.FTP.Exceptions.FileDoesntExistException;
import STRI.GroupeC.FTP.Exceptions.NotADirectoryException;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

/**
 * Classe de méthodes statiques pour gérer le système de fichier local
 */
public class LocalFileSystem {

    /**
     * Dossier partagé par le serveur
     */
    public static String SHARE_FOLDER_PATH = System.getProperty("user.home");

    /**
     * Renvoie la liste des fichiers d'un dossier donné
     * @param workingDir Dossier dans lequel chercher
     * @return Liste des fichiers dans workingDir
     * @throws FileDoesntExistException Si le dossier n'existe pas
     */
    public static File[] listFiles(String workingDir) throws FileDoesntExistException {
        File folder = new File(SHARE_FOLDER_PATH + workingDir);
        if (folder.exists()) return folder.listFiles();
        else throw new FileDoesntExistException();
    }

    /**
     * Récupère le fichier demandé
     * @param filePath Chemin vers le fichier
     * @return Le fichier en question
     */
    public static File getFile(String filePath) {
        if (!SHARE_FOLDER_PATH.endsWith(File.separator) && !filePath.startsWith(File.separator)) return new File(SHARE_FOLDER_PATH + File.separator + filePath);
        else return new File(SHARE_FOLDER_PATH + filePath);
    }

    /**
     * Vérifie que le path donné est un dossier accessible en lecture
     * @param path Chemin vers le dossier
     * @return True si c'est un dossier et que l'utilisateur à le droit de lecture
     */
    public static boolean checkDirectoryReadable(String path) {
        File dossier = getFile(path);
        return dossier.isDirectory() && dossier.canRead();
    }

    /**
     * Permet de se déplacer dans l'arborescence
     * @param path Chemin vers le nouvel emplacement
     * @throws FileDoesntExistException si le dossier/chemin n'existe pas
     * @throws NotADirectoryException si le chemin pointe vers un fichier
     * @throws CantReadException si l'utilisateur n'as pas la permission de lire dans le dossier
     */
    public static void checkChangeDirectory(String path) throws FileDoesntExistException, NotADirectoryException, CantReadException {
        File newFolder = LocalFileSystem.getFile(path);
        if (!newFolder.exists()) throw new FileDoesntExistException();
        else if (!newFolder.isDirectory()) throw new NotADirectoryException();
        else if (!newFolder.canRead()) throw new CantReadException();
    }

    /**
     * Lis un block donné dans le fichier
     * @param blockNbr Numéro du fichier à lire
     * @param fichier Ficher à lire
     * @return tableau de bytes lus
     * @throws IOException En cas de problème I/O
     */
    public static byte[] readBlockFromFile(long blockNbr, File fichier) throws IOException {

        FileInputStream contenuFichier = new FileInputStream(fichier); // On ouvre le contenu du fichier
        long tailleFichier = contenuFichier.getChannel().size();
        long offset = Networking.PACKET_SIZE * blockNbr; // On calcule le décalage pour lire le bon morceau de fichier

        if(offset > tailleFichier) { // On demande de lire après la fin du fichier donc on sort
            contenuFichier.close();
            throw new EOFException();
        }

        contenuFichier.skipNBytes(offset); // On se décale pour lire le bon morceau de fichier
        byte[] block = contenuFichier.readNBytes((int) Math.min(Networking.PACKET_SIZE, tailleFichier - offset)); // On lit avec au maximum un morceau de Networking.PACKET_SIZE octets.
        contenuFichier.close();
        return block;
    }


    /**
     * Ecris une adresse dans un fichier de config
     * @param path Chemin vers le fichier de config
     * @param addr adresse à écrire
     * @throws IOException En cas de problème d'entrées/sorties
     */
    public static void writeConfig(String path, InetSocketAddress addr) throws IOException {

        if (addr == null) addr = new InetSocketAddress(InetAddress.getLoopbackAddress(), 5050);
        FileOutputStream fileWriter = new FileOutputStream(path); // On crée un fichier
        fileWriter.write(addr.getHostName().getBytes(StandardCharsets.UTF_8));
        fileWriter.write(' ');
        fileWriter.write(Integer.toString(addr.getPort()).getBytes(StandardCharsets.UTF_8));
        fileWriter.write('\n');
        fileWriter.close();

    }


    /**
     * Lis une adresse depuis un fichier de config
     * @param path Chemin vers le fichier de config
     * @param port Port d'écoute du node
     * @return adresse lues
     * @throws IOException En cas de problème d'entrées/sorties
     */
    public static InetSocketAddress readConfig(String path, int port) throws IOException {

        try {
            FileInputStream fileReader = new FileInputStream(path);
            String[] configArray = (new String(fileReader.readAllBytes())).split("\n");
            fileReader.close();

            String[] lineSplit = configArray[0].split(" ");
            try {
                InetSocketAddress addr = new InetSocketAddress(InetAddress.getByName(lineSplit[0]), Integer.parseInt(lineSplit[1]));

                try {

                    String reponse = Networking.askToPair(addr, "JOIN " + port);
                    if (reponse.equalsIgnoreCase("ok")) return addr;
                    else {
                        System.err.println("Erreur lors du contact avec le tracker: " + reponse);
                        return null;
                    }
                } catch (IOException e) {
                    return null;
                }
            } catch (UnknownHostException e) {
                System.err.println("\nImpossible de déterminer l'addresse correspondante à ce nom de domaine : " + e.getMessage() + "\nNode non ajouté à la liste.");
                return null;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
            System.err.println("Problème de lecture du fichier de config");
            return null;
        }
    }
}
