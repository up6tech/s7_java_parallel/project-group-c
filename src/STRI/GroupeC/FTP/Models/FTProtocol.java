package STRI.GroupeC.FTP.Models;

import STRI.GroupeC.FTP.Exceptions.CantReadException;
import STRI.GroupeC.FTP.Exceptions.FileDoesntExistException;
import STRI.GroupeC.FTP.Exceptions.NotADirectoryException;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FTProtocol {

    /**
     * Message affiché en cas de demande d'aide
     */
    private static final String HELP_MSG =
"""
Available commands:
HELP    Show this help message
LIST    List all files in current directory
PWD     List path of current directory
CWD     Change current directory
RETR    Download a remote file
HAS     Check if a file exists
QUIT    Quit the program
""";

    /**
     * Point d'entrée du traitement du protocole FTP
     * @param command Chaine de charactère reçue du client
     * @return Chaine à renvoyer au client
     * @throws RuntimeException En cas de demmande de fin de connexion l'exception comporte le message "QUIT"
     */
    public static String commandParser(String command) throws RuntimeException {
        String[] request = command.split(" ", 2);
        try {
            return switch (request[0].toLowerCase()) {
                case "help" -> HELP_MSG;
                case "list" -> commandLIST(request[1]);
                case "cwd" -> commandCWD(request[1]);
                case "retr" -> commandRETR(request[1]);
                case "has" -> commandHAS(request[1]);
                case "quit", "" -> throw new RuntimeException("QUIT"); // On déclenche une exception pour détecter une demande de fin de connection
                default -> "ERROR - UnknownCommand\n" + HELP_MSG;
            };
        } catch (ArrayIndexOutOfBoundsException e) {
            return "ERROR - Missing argument";
        }
    }

    /**
     * Traitement de l'affichage du contenu du dossier courant
     * @param workingDir Dossier courant
     * @return Contenu du dossier sous forme de chaine de charactères
     */
    private static String commandLIST(String workingDir) {

        File[] fichiers;
        try {
            fichiers = LocalFileSystem.listFiles(workingDir);
        } catch (FileDoesntExistException e) { return "ERROR - File not found"; }

        StringBuilder sortie = new StringBuilder(workingDir).append("\nD - ..");
        List<String> nomsFichiers = new ArrayList<>();
        for (File fic : fichiers) {
            nomsFichiers.add("\n" + (fic.isDirectory()? "D" : "F") + " - " + fic.getName());
        }
        return sortie.append(nomsFichiers.stream().sorted().collect(Collectors.joining())).toString();
    }

    /**
     * Traitement du changement du dossier
     * @param newFolder Chemin vers le nouveau dossier
     * @return "OK" ou l'erreur correspondante
     */
    private static String commandCWD(String newFolder) {

       try {
           LocalFileSystem.checkChangeDirectory(newFolder);
       } catch (FileDoesntExistException ignored) {
           return "ERROR - Folder not found";
       } catch (NotADirectoryException ignored) {
            return "ERROR - This is not a folder";
        } catch (CantReadException ignored) {
           return "ERROR - Can't read folder content";
       }
        return "OK";
    }

    /**
     * Traitement de l'envoi de fichier
     * @param request Arguments de la commande RETR
     * @return "OK" ou l'erreur correspondante
     */
    private static String commandRETR(String request) {

        long blockNbr = Long.parseLong(request.split(" ", 2)[0]);
        String filePath = request.split(" ", 2)[1];
        File fichier = LocalFileSystem.getFile(filePath);
        if (!fichier.exists()) return "ERROR - File not found";
        else if (fichier.isDirectory()) return "ERROR - This is a folder";
        else if (!fichier.canRead()) return "ERROR - Can't read file content";
        if (blockNbr * Networking.PACKET_SIZE > fichier.length()) return "END";
        try {
            ServerSocket listeningdataSocket = new ServerSocket(0);
            new Thread(() -> {
                try {
                    Socket dataSocket = listeningdataSocket.accept();
                    dataSocket.getOutputStream().write(LocalFileSystem.readBlockFromFile(blockNbr, fichier));
                    dataSocket.getOutputStream().flush();
                    dataSocket.close();
                    listeningdataSocket.close();
                } catch (IOException ignored) {}
            }).start();
            return "OK_DATA_PORT " + listeningdataSocket.getLocalPort();
        } catch (IOException e) { return "ERROR - Unable opening data socket"; }
    }

    /**
     * Permet de vérifier à distance si un fichier existe (utilisé par le tracker)
     * @param path Chemin du fichier
     * @return "OK" si le fichier existe "NO" sinon
     */
    private static String commandHAS(String path) {
        if (LocalFileSystem.getFile(path).exists()) return "OK";
        else return "NO";
    }
}