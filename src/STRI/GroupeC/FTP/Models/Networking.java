package STRI.GroupeC.FTP.Models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Classe en charge de la manipulation du réseau de façon générale
 */
public class Networking {

    /**
     * Taille maximale d'un packet lors du transfert d'un fichier
     */
    public static final int PACKET_SIZE = 4096;

    /**
     * Permet de lire le socket facilement
     * @param streamToRead Stream sur lequel lire
     * @return Données lues
     * @throws IOException En cas d'erreur de socket
     */
    public static String lireSocket(BufferedReader streamToRead) throws IOException {
        String serverAnswer = "";
        do {
            try {
                serverAnswer += (streamToRead.readLine().concat("\n"));
            } catch (NullPointerException ignored) {
                break;
            }
        } while (serverAnswer.charAt(serverAnswer.length()-2) != '\u001a' && !serverAnswer.endsWith("null\n"));

        if (serverAnswer.endsWith("null\n")) return serverAnswer.substring(0, Math.max(0, serverAnswer.length() - 5));
        else return serverAnswer.substring(0, Math.max(0, serverAnswer.length() - 2));
    }

    /**
     * Permet d'écrire dans le socket facilement pour que ce soit conforme pour {@link Networking#lireSocket(BufferedReader)}
     * @param streamToWrite Stream sur lequel écrire
     * @param message Texte à envoyer sur le socket
     */
    public static void ecrireSocket(PrintStream streamToWrite, String message) {
        streamToWrite.println(message + '\u001a');
    }

    /**
     * Sélection et connexion à un serveur aléatoire
     * @param serveurs Liste des addresses des serveurs connus
     * @return Socket vers un serveur
     */
    public static Socket selectionServeur(List<InetSocketAddress> serveurs, Random randomGenerator) {
        int indexSocket; // Index du socket dans la liste de tous les serveurs
        Socket socket = null;
        BufferedReader lectureSocket = null;
        PrintStream ecritureSocket = null;
        do {
            indexSocket = randomGenerator.nextInt(0, serveurs.size()); // On choisit un serveur au hasard pour chaque commande
            try {
                socket = new Socket(serveurs.get(indexSocket).getAddress(), serveurs.get(indexSocket).getPort());
                lectureSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                ecritureSocket = new PrintStream(socket.getOutputStream());
            } catch (IOException e) {
                System.err.println("\nErreur de socket : " + e.getMessage() +
                        "\nOn retire " + serveurs.get(indexSocket).getAddress() + ":" +
                        serveurs.get(indexSocket).getPort() + " de la liste des serveurs");
                serveurs.remove(indexSocket);
            }
        } while (lectureSocket == null || ecritureSocket == null); // Boucle en cas de pb de socket.
        return socket;
    }

    /**
     * Simplifie la connexion et l'envoi vers un pair
     * @param pairAddr Adresse du serveur à contacter
     * @param message Message à envoyer
     * @return Message reçu en réponse
     * @throws IOException En cas de problème de socket
     */
    public static String askToPair(InetSocketAddress pairAddr, String message) throws IOException {

        Socket socket;
        try {
             socket = new Socket(pairAddr.getAddress(), pairAddr.getPort());
        } catch (IOException e) {
            System.err.println("Impossible de se connecter: " + e.getMessage());
            System.err.println("Commande annulée");
            throw e;
        }
        return Networking.askToPair(socket, message);
    }

    /**
     * Simplifie la connexion et l'envoi vers un pair
     * @param socket Socket à utiliser
     * @param message Message à envoyer
     * @return Message reçu en réponse
     * @throws IOException En cas de problème de socket
     */
    public static String askToPair(Socket socket, String message) throws IOException {

        try {
            BufferedReader lectureSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintStream ecritureSocket = new PrintStream(socket.getOutputStream());

            Networking.ecrireSocket(ecritureSocket, message);
            String reponse = Networking.lireSocket(lectureSocket);
            socket.close(); // Fermeture propre
            return reponse;
        } catch (IOException e) {
            System.err.println("Problème de communication: " + e.getMessage());
            System.err.println("Commande annulée");
            throw e;
        }
    }

    /**
     * Parse une InetSocketAddress au format "192.168.1.1 1234" ou "example.com 1234"
     * @param line Ligne à parser
     * @return InetSocketAddress correspondante
     */
    public static InetSocketAddress parseSocketAddr(String line) {
        if (line.startsWith("ERROR")) {
            System.err.println(line);
            return null;
        }
        String[] splitedLine;
        if (line.endsWith("\n")) splitedLine = line.substring(0, line.length() - 1).split(" ");
        else splitedLine = line.split(" ");
        if (splitedLine.length != 2) return null;
        try {
            return new InetSocketAddress(InetAddress.getByName(splitedLine[0]), Integer.parseInt(splitedLine[1]));
        } catch (UnknownHostException e) {
            System.err.println("\nImpossible de déterminer l'addresse correspondante à ce nom de domaine : " + e.getMessage());
            return null;
        }
    }

    /**
     * Parse plusieurs InetSocketAddress
     * @param lines Lignes à lire
     * @return Liste des adresses
     */
    public static ArrayList<InetSocketAddress> parseSocketAddr(String[] lines) {
        ArrayList<InetSocketAddress> output = new ArrayList<>();
        for (String line: lines) {
            InetSocketAddress addr = Networking.parseSocketAddr(line);
            if (addr != null) output.add(addr);
        }
        if (output.size() == 0) return null;
        else return output;
    }

    /**
     * Télécharge un block de données
     * @param addr Adresse du serveur
     * @param port Port du serveur
     * @return Tableau de bytes
     * @throws IOException En cas de problème de socket
     */
    public static byte[] downloadDataBlock(InetAddress addr, int port) throws IOException {
        Socket fileSocket = new Socket(addr, port);
        byte[] readedBytes = fileSocket.getInputStream().readAllBytes(); // On récupère tout le contenu
        fileSocket.close(); // On ferme le socket quand on a lu
        return readedBytes;
    }
}
