package STRI.GroupeC.FTP;

import STRI.GroupeC.FTP.Models.FTProtocol;
import STRI.GroupeC.FTP.Models.LocalFileSystem;
import STRI.GroupeC.FTP.Models.Networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurFTP {

    private ServerSocket socketEcoute;

    public ServeurFTP(int port, String sharePath) {

        if (sharePath != null) {
            LocalFileSystem.SHARE_FOLDER_PATH = sharePath; // On récupère le chemin du dossier
            if (!LocalFileSystem.checkDirectoryReadable("")) { // Si le dossier n'est pas acessible on fallback sur le dossier home.
                System.err.println("\nLe dossier \"" + LocalFileSystem.SHARE_FOLDER_PATH + "\" est innacessible. Utilisation du dossier home de l'utilisateur.");
                LocalFileSystem.SHARE_FOLDER_PATH = System.getProperty("user.home");
            }
        } else LocalFileSystem.SHARE_FOLDER_PATH = System.getProperty("user.home");

        // On essaie d'ouvrir le port 21
        try {
            this.socketEcoute = new ServerSocket(port);
        } catch (IOException e) {
            try { this.socketEcoute = new ServerSocket(0);} // Si ça échoue on laisse l'OS choisir
            catch (IOException ex) {
                System.out.println("Impossible d'ouvrir un socket d'écoute: " + ex.getMessage());
                return;
            }
        }
        System.out.println("Serveur en écoute sur le port: " + this.socketEcoute.getLocalPort());

        new Thread(() -> {
            try {
                while (true) {
                    Socket socketClient = socketEcoute.accept();
                    new Thread(() -> {
                        try { gestionClient(socketClient); }
                        catch (IOException e) { e.printStackTrace(); }
                    }).start();
                }
            } catch (IOException ignored) {}
        }).start();
    }

    public void stopListening() throws IOException {
        this.socketEcoute.close();
    }

    /**
     * Méthode appellée par le Thread de gestion des clients.
     * @param socket socket de connexion avec le client
     * @throws IOException en cas de problème de lecture du socket
     */
    public static void gestionClient(Socket socket) throws IOException {

        // On instancie les objets pour manipuler le socket
        BufferedReader entreeClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintStream sortieClient = new PrintStream(socket.getOutputStream());

        String commandeClient; // Première lecture de l'input

        // On boucle tant que le client n'as pas fermé son côté du socket
        do {
            commandeClient = Networking.lireSocket(entreeClient); // Lecture de l'input
            try {
                Networking.ecrireSocket(sortieClient, FTProtocol.commandParser(commandeClient)); // Traitement de la requête
            // On intercepte la demande de fin
            } catch (RuntimeException e) {
                if (e.getMessage().equals("QUIT")) break;
                else throw e;
            }
        } while(!commandeClient.equals(""));
        // On a fini on ferme proprement
        socket.close();
    }
}
