package STRI.GroupeC.FTP;

import STRI.GroupeC.FTP.Models.LocalFileSystem;
import  STRI.GroupeC.FTP.Models.Networking;
import STRI.GroupeC.P2P.NodeP2P;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.stream.Stream;

public class ClientFTP {

    /**
     * Programme principal de la section Client
     * @param tracker Adresse du trackeur de coordination
     */
    public static void run(InetSocketAddress tracker) {

        Scanner clavier = new Scanner(System.in); // Lecture clavier
        String[] luClavier;
        String commandPath; // Chemin complet à transmettre dans la requete
        String typeCommande; // Commande sans les arguments
        String remotePath = "/"; // Chemin relatif au sein du dossier distant (donc un / fait référence à la racine du dossier distant et pas du système de fichier de la machine distante)

        System.out.println("Saisissez vos commandes (HELP pour de l'aide)");

        /*
         * ETAPE 2: ON TRAITE LES COMMANDES
         */
        while (true) { // Boucle principale on sort avec un QUIT

            luClavier = clavier.nextLine().split(" ", 2); // Lecture du clavier
            typeCommande = luClavier[0].toLowerCase(); // On découpe pour ne garder que le premier mot
            if (luClavier.length > 1) {
                if (luClavier[1].startsWith("/")) commandPath = luClavier[1];
                else {
                    if (!remotePath.endsWith("/")) commandPath = remotePath + "/" + luClavier[1];
                    else commandPath = remotePath + luClavier[1];
                }
            } else commandPath = remotePath;


            switch (typeCommande) {
                case "pwd" -> {
                    System.out.println(remotePath);
                    continue;
                }
                case "quit" -> { return; }
                default -> {}

            }

            try {

                switch (typeCommande) {
                    case "retr" -> {
                        String fileName = luClavier[1];
                        String finalRemotePath = remotePath;
                        new Thread(() -> gestionDownloads(tracker, finalRemotePath, fileName)).start(); // On lance un Thread qui s'occupe de récupérer le fichier.
                    }
                    case "list" -> listAllRemoteFolders(tracker, typeCommande, commandPath, NodeP2P.serverListenningPort);
                    case "cwd" -> {
                        InetSocketAddress pair = Networking.parseSocketAddr(Networking.askToPair(tracker, "WHERE " + typeCommande + " " + NodeP2P.serverListenningPort + " " + commandPath));
                        if (pair == null) {
                            System.err.println("Aucun autre pair sur ce TrackerP2P");
                            continue;
                        }
                        String recuServeur = Networking.askToPair(pair, typeCommande + " " + commandPath);
                        if (recuServeur.equals("OK")) { // Si le serv valide on applique le changement
                            if (luClavier[1].startsWith("/")) remotePath = luClavier[1];
                            else {
                                if (!remotePath.endsWith("/")) remotePath += "/";
                                remotePath += luClavier[1];
                            }
                            if (remotePath.equals("/")) remotePath = "";
                        } else System.err.println("\n" + recuServeur);
                    }
                    default -> {
                        InetSocketAddress pair = Networking.parseSocketAddr(Networking.askToPair(tracker, "WHERE " + typeCommande + " " + NodeP2P.serverListenningPort + " " + commandPath));
                        if (pair == null) {
                            System.err.println("Aucun autre pair sur ce TrackerP2P");
                            continue;
                        }
                        System.out.println(Networking.askToPair(pair, typeCommande + " " + commandPath)); // On se contente d'afficher
                    }
                }
            } catch (IOException e) {
                System.err.println("\nErreur de socket : " + e.getMessage());
            }
        }
    }

    /**
     * Liste les fichiers sur tous les serveurs distants
     * @param tracker Adresse du tracker
     * @param commandeType Commande avec chemin demandé
     * @param remotePath Chemin sur le dossier distant
     * @param port Port d'écoute de notre node pour les stats
     */
    private static void listAllRemoteFolders(InetSocketAddress tracker, String commandeType, String remotePath, int port) {

        try {
            ArrayList<InetSocketAddress> pairsP2P = Networking.parseSocketAddr(Networking.askToPair(tracker, "WHERE " + commandeType + " " + port + " " + remotePath).split("\n"));
            if (pairsP2P == null) {
                System.err.println("Aucun autre pair sur ce TrackerP2P");
                return;
            }
            ArrayList<String> reponses = new ArrayList<>();
            pairsP2P.forEach( addr -> {
                try {
                    String reponse = Networking.askToPair(addr, commandeType + " " + remotePath);
                    if (!reponse.equals("ERROR - File not found")) Collections.addAll(reponses, reponse.split("\n"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            System.out.println(String.join("\n", Stream.of(reponses).flatMap(List::stream).distinct().sorted().toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gestion complète du téléchargement d'un fichier donné
     *
     * @param tracker Adresse du tracker
     * @param path     Chemin du fichier à récupérer
     * @param fileName Nom du fichier
     */
    private static void gestionDownloads(InetSocketAddress tracker, String path, String fileName) {

        int currentBlockNbr = 0;
        FileOutputStream fileWriter;
        try {
            fileWriter = new FileOutputStream(LocalFileSystem.getFile(fileName)); // On crée un fichier
        } catch (FileNotFoundException e) {
            System.err.printf("\nImpossible de télécharger le fichier %s. Sur le dossier local c'est déjà un dossier.%n\n", fileName);
            return;
        } catch (SecurityException e) {
            System.err.println("\nImpossible d'écrire dans ce dossier.");
            return;
        }

        String completePath;
        if (path.endsWith(File.separator) || path.startsWith(File.separator)) completePath = path + fileName;
        else completePath = path + File.separator + fileName;

        do {
            try {

                InetSocketAddress pairAddr = Networking.parseSocketAddr(Networking.askToPair(tracker, String.format("RETR %d %s", NodeP2P.serverListenningPort, completePath)));
                if (pairAddr == null) {
                    System.err.println("Aucun autre pair sur ce TrackerP2P");
                    continue;
                }
                String reponseServeur = Networking.askToPair(pairAddr, String.format("RETR %d %s", currentBlockNbr, completePath));
                switch (reponseServeur.split(" ", 2)[0].toLowerCase()) {
                    case "ok_data_port":
                        byte[] block = Networking.downloadDataBlock(pairAddr.getAddress(), Integer.parseInt(reponseServeur.split(" ", 2)[1]));
                        fileWriter.write(block); // On écrit le tout sur le disque
                        fileWriter.flush(); // On attend que tout soit bien écrit
                        break;
                    case "end":
                        currentBlockNbr = -10; // Tout a été téléchargé on sort de la boucle
                        break;
                    case "error":
                    default:

                        if (!reponseServeur.equals("ERROR - File not found")) { // Si un erreur surviens autre qu'un fichier perdu
                            fileWriter.close();
                            LocalFileSystem.getFile(fileName).delete();
                            System.err.println("\n" + reponseServeur);
                            return;
                        }
                }
                currentBlockNbr++;
            } catch (IOException ignored) { }
        } while (currentBlockNbr >= 0);
        try {
            fileWriter.close(); // On peut fermer le tout
        } catch (IOException ignored) {
        }
        System.out.println("Fichier " + fileName + " téléchargé.");
    }
}
